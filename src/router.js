import Vue from 'vue'
import Router from 'vue-router'
import './views/Teams.vue'
import './views/Fencers.vue'
import './views/Fencer.vue'

Vue.use(Router)

export default new Router({
  mode: 'history',
  base: process.env.BASE_URL,
  routes: [
    {
      path: '/',
      name: 'home',
      component: () => import(/* webpackChunkName: "about" */ './views/Home.vue')
    },
    {
      path: '/fencers',
      name: 'fencers',
      component: () => import(/* webpackChunkName: "about" */ './views/Fencers.vue')
    },
    {
      path: '/fencer/:id',
      name: 'fencer',
      component: () => import(/* webpackChunkName: "about" */ './views/Fencer.vue')
    },
    {
      path: '/teams',
      name: 'teams',
      // route level code-splitting
      // this generates a separate chunk (about.[hash].js) for this route
      // which is lazy-loaded when the route is visited.
      component: () => import(/* webpackChunkName: "about" */ './views/Teams.vue')
    },
    {
      path: '/team/:id',
      name: 'team',
      component: () => import(/* webpackChunkName: "about" */ './views/Team.vue')
    }
  ]
})
